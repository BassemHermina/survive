const path = require('path');

module.exports = {
  mode: 'development', //production
  entry: './dist-server/game-engine/main.js',
  output: {
    filename: 'game-engine.bundle.js',
    path: path.resolve(__dirname, './public/build'),
  },
  performance: { // this disables the warning for having a large file (threejs library included in the engine)
    hints: false
  }
};