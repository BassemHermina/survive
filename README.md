# survive.io

an FPS game based on webGL and ThreeJS

--- 
### starting the project
- npm init
- npm install express, three
- how to use install/babel to 'transpile' ES6 to ES5 https://www.freecodecamp.org/news/how-to-enable-es6-and-beyond-syntax-with-node-and-express-68d3e11fe1ab/ <3
- public folder is the static served directory 

tyb el bta3 da shaghal ezay:
el awl ktabt el index.js be express zy l ktab, b3d kda 3mlt file tani esmo router we 
7atet fih l logic bta3 el routing we hwa el b2a bywaza3 min hy3ml a 3ashan el index.js
yb2a lel configurations bs. 
b3d kda 7awelt el require le import we olt ana haktb ES6 fa dawart we la2et el link
el fo2 we shaghalt babel be npm run transpile eno ya5od el code el fi folder server
we y3mlo compile ito folder dist-server (.gitignore)
b3d kda 3mlt folder lel views, we da kan a7sn tari2a a render biha el html 3ashan 
a3ml compile lel js code bta3i, we 3malt dust engine , we 3mlt main.dust 3adya kda
we kol l files el hya btst5dmha ma7tota fi el public folder el m3molo host static files
tb da fady, min bymlah ? el bundler bya5od el output bta3 babel b3d l compile we ygama3o fi 
file wa7ed we y7oto fl public
^ dlwa2ty 7ateet el bundler el esmo webpack https://webpack.js.org/guides/getting-started/
we 5aleeto (ba3d ma a3ml build to es2015 be babel) ya5od el entry point (maktoba fi webpack.config.js)
we y3ml bundle lel file da be kol l by7tago automatic , we tb3n el by7tago da by7tawi 3la el three.js 
library bta3etna \o/ fa bytla3 file wa7ed js byt7at fl public esmo game-engine el client bya5do we bs kda ! 
wooowww!
dlwa2ty el by7sl fl code hna 3 steps, bakteb l awel l code be es2016
step1: ba3ml build lel js files be babel we bytla3o fi folder dist-server
step2: ba3ml bundle lel client files el tal3a fi dist-server/game-engine we byt7t fi public
step3: run l server b2a. wel server by-serve el router el by render template be dust, btesta5dem el file l fi public as a client js file
we barak alah fima razak :V ektb da english b2a
