
import express from 'express';
import path from 'path';
import { router } from './router';


// initialization
var app = express();
app.engine('dust', require('dust-engine').renderForExpress);
app.set('view engine', 'dust');
// app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('public'));

// routing
router(app);

// configuration and server code
app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'), function(){
    console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.');
});