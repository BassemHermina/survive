/**
 * Gamescene Class
 * @author BassemHermina
 * Date: 7 Sep 2020
 */
import * as THREE from 'three';

export class Loader{

    constructor(){
        // 
    }
}

Loader.meshnormalmaterial = new THREE.MeshNormalMaterial();
Loader.meshnormalmaterial_instanced = new THREE.MeshNormalMaterial(); // TODO! Threejs treats all the 'MeshNormalMaterial' instances as one!
Loader.meshbasicmaterial = new THREE.MeshBasicMaterial( {color: 0xffff00} );
Loader.meshbasicmaterial_instanced = new THREE.MeshBasicMaterial( {color: 0xffff00} );

Loader.boxgeometry = new THREE.BoxGeometry(1,1,1); // no use?
Loader.boxgeometry_instanced = new THREE.BoxGeometry(1,1,1);
Loader.spheregeometry = new THREE.SphereGeometry(1, 8, 8);
Loader.spheregeometry_instanced = new THREE.SphereGeometry(1, 8, 8);

