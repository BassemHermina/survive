/**
 * Physicsobject Class
 * @author BassemHermina
 * Date: 23 Sep 2020
 */
import * as THREE from 'three';

export class Physicsobject{


    constructor(owner){
    
        // declaration

        /**
         * reference to the attached game object
         */
        this.owner = null;
        // todo: all owners (player class, gameobject, ..) shall implemenet the same interface functions 
        // owner.getgeometry: for creating the bounding box, returns array of geometries to create array of AABBs

        /**
         * array holding the Threejs/custom bounding volume/s for physics interaction
         * IMPORTANT: note that the AABB limits the world map objects into only axis aligned objects, created from boxes, mostly concave
         * https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_collision_detection/Bounding_volume_collision_detection_with_THREE.js
         */
        this.boundingvolume = [];

        /**
         * boolean to enable gravity effect on attached object
         */
        this.enablegravity = false;

        /**
         * boolean to enable physics interaction with other object and diallow objects passing through this object
         */
        this.solid = true;

        /**
         * value used for simulating gravity when object is not onground
         */
        this.downwardvelocity = 0; 
        
        /**
         * boolean set to true if ray shooted down intersects with an object on some pre-set distance
         */
        this.onground = false;


        /////////////////////////////////////
        // initialization
        this.owner = owner;
        this.boundingvolume = this.owner.getmeshes().map( mesh => this.createAABB(mesh) )
        if (!this.boundingvolume.length) this.solid = false;
    }

    createAABB(mesh){          
        let AABB = new THREE.Box3(new THREE.Vector3(), new THREE.Vector3());
        AABB.setFromObject(mesh);
        return AABB;
    }

}
