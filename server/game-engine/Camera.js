
/**
 * Camera Class
 * @author BassemHermina
 * Date: 25 Aug 2020
 */

import { PerspectiveCamera } from 'three';
import { OrbitControls } from './three_examples/OrbitControls';
import { PointerLockControls } from './three_examples/PointerLockControls';
import * as THREE from 'three';

import config from './game-config.js'
import { Player } from './Player';

const CAMERA_FIRST_PERSON_STEP_TRIGONOMETRIC_DIVIDER = 6;
const CAMERA_FIRST_PERSON_STEP_DIVIDER = 40;
const CAMERA_FIRST_PERSON_WAVING_DIVIDER = 400;
const CAMERA_FIRST_PERSON_MOVEMENT_SPEED = 1.1;
const EYE_POSITION = config.worldmap.camerafirstperson.y;

export class Camera{

    constructor(threerenderer){      
            
      // declaration
      /**
       * the Threejs camera object
       */
      this.camera;

      /**
       * the player model (containing the physics component)
       */
      this.myplayer;

      /**
       * for defining debug-controls/ingame-controls/disable-controls 
       */
      this.controlsmode;
      this.controls;

      /**
       * this is the webgl threejs renderer, sometimes it is needed in the camera
       */
      this.activescenerenderer;

      /**
       * first person camera ingame controls
       */
      this.moveForward = false;
			this.moveBackward = false;
			this.moveLeft = false;
      this.moveRight = false;
      this.velocity = new THREE.Vector3();
      this.direction = new THREE.Vector3();
      this.swingcounter = 0;
      this.breathcounter = 0;
      this.cameraheight_withoutswing = EYE_POSITION;
      ////////////////////////////////////////////



      // initialization
      this.camera = new PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 1000 );
      this.controlsmode = config.globalconf.gamemode;
      this.activescenerenderer = threerenderer;
      this.myplayer = new Player();

      this.initializecameracontrols();
    }

    initializecameracontrols(){
      switch (this.controlsmode){
        case 'debug':
          this.controls = new OrbitControls( this.camera, this.activescenerenderer.domElement );
          this.controls.enableDamping = true; 
          this.controls.dampingFactor = 0.05;
          Object.assign(this.camera.position, config.worldmap.cameradebug)
          this.controls.update();
          break;
        case 'ingame':
          this.controls = new PointerLockControls( this.camera, this.activescenerenderer.domElement );
          Object.assign(this.camera.position, config.worldmap.camerafirstperson)
          this.activescenerenderer.domElement.addEventListener( 'click',  () => this.controls.lock() , false );
          document.addEventListener( 'keydown', (event) => {
            switch(event.keyCode){
              case 87: // w
							  this.moveForward = true;   break;
						  case 65: // a
                this.moveLeft = true;      break;
						  case 83: // s
                this.moveBackward = true;  break;
						  case 68: // d
                this.moveRight = true;     break;
              case 32: // space
              // hack to implement jumping
                this.myplayer.physicsobject.downwardvelocity +=0.03;
                this.cameraheight_withoutswing+=0.02
                break;
            }
          }, false );
          document.addEventListener( 'keyup', (event) => {
            switch(event.keyCode){
              case 87: // w
                this.moveForward = false;   break;
						  case 65: // a
                this.moveLeft = false;      break;
						  case 83: // s
                this.moveBackward = false;  break;
						  case 68: // d
                this.moveRight = false;     break;
            }
          }, false );
          this.initializecrosshair();
          break;
        case 'disable':
          // this disable-controls mode have two variants
          // dead, or watching through another player's camera (his control parent the camera object)
          break;
      }
    }

    update(){

      // updates camera position (player's eye) based on controls and physics interaction with world

      switch(this.controlsmode){
        case 'debug':
          this.camera.controls.update();
          break;
        
        case 'ingame':
          // losing velocity
          this.velocity.x -= this.velocity.x * 0.5;
          this.velocity.z -= this.velocity.z * 0.5;
          
					this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
					this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
					this.direction.normalize(); // this ensures consistent movements in all directions

          // gaining velocity
          if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * 0.01 * CAMERA_FIRST_PERSON_MOVEMENT_SPEED ;
					if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * 0.01 * CAMERA_FIRST_PERSON_MOVEMENT_SPEED ;


          // set camera position as this.positonbackup to remove the swingning factor
          this.camera.position.y = this.cameraheight_withoutswing;

          this.controls.moveRight( - this.velocity.x );
          this.controls.moveForward( - this.velocity.z );
          //
          // use physics in the player class to update and correct the movement based on world
          this.myplayer.mirrorposition(this.camera);
          Object.assign(this.camera.position, this.myplayer.updatepositionusingphysics());
          //
          // set the this.position to the new value to save it for the next frame
          this.cameraheight_withoutswing = this.camera.position.y;

          // BUG! where is the player initial height is set? this will show up when i try implement jumping

          // calculate the swinging factor and add it camera current position 
          // Used for 1st person swinging movement 
          if ((this.moveForward || this.moveBackward || this.moveLeft || this.moveRight) && this.myplayer.physicsobject.onground ) {
            this.swingcounter++;
            this.camera.rotateZ(Math.sin(this.swingcounter/(CAMERA_FIRST_PERSON_STEP_TRIGONOMETRIC_DIVIDER*1.6) )/(CAMERA_FIRST_PERSON_WAVING_DIVIDER*2));
          }
          this.camera.position.y -= 1.9*Math.sin(this.swingcounter/CAMERA_FIRST_PERSON_STEP_TRIGONOMETRIC_DIVIDER*1.4)/(CAMERA_FIRST_PERSON_STEP_DIVIDER*2);
          
          this.breathcounter++;
          this.camera.rotateX(Math.sin(this.breathcounter/(CAMERA_FIRST_PERSON_STEP_TRIGONOMETRIC_DIVIDER*3))/(CAMERA_FIRST_PERSON_WAVING_DIVIDER*5));
          break;
      }
    }

    initializecrosshair(){
      
    }


}