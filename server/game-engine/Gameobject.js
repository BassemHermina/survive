/**
 * Gameobject Class
 * @author BassemHermina
 * Date: 5 Sep 2020
 */
import * as THREE from 'three';
import { OBJLoader } from './three_examples/OBJLoader';
import { Physicsobject } from './Physicsobject';

export class Gameobject{
    
    /**
     * Has all the threejs properties needed to create and render a mesh/object and animate it plus 
     * having extra options for world interactions and event handling
     */
    constructor(b = null, m = null){
        this.geometryBuffer = b;
        this.material = m;
        this.mesh = null;
        this.physicsobject = null;
        this.instanceof = null; 
        
        this.pos = {};
        this.rot = {};
        this.scale = {};
    }
    
    // should this be here?
    // I think there should be a static class for loading objects and materials, and having 
    // the loaded data as global static to share them between different game objects (for instancing)
    // so as to not load the same model every time an object is instanciated with it
    // also note Threejs docs mentioned that it's not correct to use the same material/geometry for 
    // insctanced meshes and normal meshes
    loadmodel(path){
        const extension = path.split('.')[1];
        switch(extension){
            case 'obj':
                const loader = new OBJLoader();
                loader.load( path, obj => this.geometryBuffer = obj.BufferGeometry );
                break;

            // other cases for other data types

            default:
                throw "Unkown model extension.";
        }
    }

    initializemesh(){
        // initialize (creates a mesh using geometry buffer and material) (called by gamescene if not going to be instanced)
        if (this.instanceof) throw("Object already initialized as instance of"+ this.instanceof);
        if (!this.geometryBuffer || !this.material) throw("Object's material/geometry is missing, can't initialize");
        this.mesh = new THREE.Mesh( this.geometryBuffer, this.material );
        this.mesh.gameobject = this;
        Object.assign(this.mesh.scale, this.scale);
        Object.assign(this.mesh.position, this.pos);

        // initialize physics object
        this.physicsobject = new Physicsobject(this);
        return this.mesh;
    }

    initializeinstancedmesh(samebufferlist){
        if (this.instanceof) throw("Object already initialized as instance of"+ this.instanceof);
        if (this.geometryBuffer || this.material) throw("Object has a geometry/material, are you sure it is an instancing group?");
        this.mesh = new THREE.InstancedMesh(
            samebufferlist[0].geometryBuffer, 
            samebufferlist[0].material, 
            samebufferlist.length
            );
        this.mesh.instanceMatrix.setUsage( THREE.StaticDrawUsage );
        this.mesh.gameobject = this;
        
        // setting the transform of the instanced meshes
        let dummy = new THREE.Object3D(), i = 0;
        samebufferlist.forEach( instance => {
            dummy.position.set( instance.pos.x, instance.pos.y, instance.pos.z );
            dummy.scale.set (instance.scale.x, instance.scale.y, instance.scale.z);
            dummy.updateMatrix();
            instance.initializeasinstance(this,i);
            this.mesh.setMatrixAt( i++, dummy.matrix );
        });
        return this.mesh;
    }

    initializeasinstance(obj, i) {
        if (this.instanceof) throw("Object already initialized as instance of " + this.instanceof);
        this.instanceof = obj;
        this.index = i;
        this.geometryBuffer, this.material = null; // why?
        this.mesh.gameobject = this;
    }

    getmeshes(){
        // only physics object calls this function.
        // and it only handles convex simple geometry (axis aligned)
        return (this.mesh ? [this.mesh] : []); 
    }
}