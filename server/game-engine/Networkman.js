/**
 * Networkman Class
 * @author BassemHermina
 * Date: 25 Aug 2020
 */

class Networkman{
    
    /**
     * One of the most important/hardest classes in the game-engine
     * 1 This class handles all the requests/responses/sockets sent and recieved from the server
     * regarding loading the obj models at the beginning or sending and recieving game data thru
     * the gameplay time
     * need to figure out best way for highest fps to send and recieve formating of the camera/players
     * movements and actions to be effecient enough
     * 
     * traffic/network related stuff (client side optimizations):
     * - client side predictions/bending/interpolation/extrapolation
     * - game state updates are accumulated within fixed time window before transmission 
     * I can ask server for new data periodically and don't have to wait for it every frame, but predict it?
     */
    constructor(){

    }
}