/**
 * Player Class
 * @author BassemHermina
 * Date: 9 Sep 2020
 */

import * as THREE from 'three';
import {Physicsobject} from './Physicsobject'
import { AlwaysStencilFunc } from 'three';

export class Player{
    
    /**
     * the player class can either send/recieve state data through the network manager to handle online game data
     */
    constructor(){
        /**
         * current position, used for calculating physics stuff
         */
        this.position;

        /**
         * handle for the gamescene, it is set by the gamescene in {createplayer()}
         */
        this.gamescene;

        /**
         * a dummy mesh to let Three handle the collision and following of the camera
         */
        this.mesh = new THREE.Mesh( new THREE.BoxGeometry(0.3,0.6,0.3), new THREE.MeshNormalMaterial() );
        this.mesh.geometry.computeBoundingBox();

        /**
         * physics object to model intersections and physics for players with world
         */
        this.physicsobject = new Physicsobject(this);

    }

    /**
     * used to let the Player mirror the movement of another object
     */
    mirrorposition(mirror){    
        this.position = mirror.position;
        this.mesh.position.set(mirror.position.x, mirror.position.y, mirror.position.z);
        this.physicsobject.boundingvolume[0].setFromObject(this.mesh);
        if (this.physicsboxhelper) this.physicsboxhelper.update()
    }

    updatepositionusingphysics(){

        for (let i = 0; i < this.gamescene.gameobjects.length; i++)
            if (this.gamescene.gameobjects[i].physicsobject.solid)
                if (this.physicsobject.boundingvolume[0].intersectsBox(this.gamescene.gameobjects[i].physicsobject.boundingvolume[0]) ){
                    // intersects another object
                    let intersectionbox3 = this.physicsobject.boundingvolume[0].clone();
                    intersectionbox3 = intersectionbox3.intersect(this.gamescene.gameobjects[i].physicsobject.boundingvolume[0]);
                    let mycenter = new THREE.Vector3();
                    this.physicsobject.boundingvolume[0].getCenter(mycenter)
                    let intersectionbox3center = new THREE.Vector3();
                    intersectionbox3.getCenter(intersectionbox3center);
                    let directionofray = intersectionbox3center.sub(mycenter);
                    directionofray.normalize()
                    let intersection = new THREE.Raycaster(mycenter, directionofray).intersectObject(this.gamescene.gameobjects[i].mesh)
                    intersectionbox3.getCenter(intersectionbox3center);
                    let vecd = intersection[0].point.sub(intersectionbox3center);
                    let magnitude = vecd.dot(intersection[0].face.normal);

                    let translationvec = intersection[0].face.normal.clone();
                    translationvec.multiplyScalar(magnitude);

                    this.position.add(translationvec)
                    // dot product the vector between the intersection point and the new center and the normal vector to get distance
                    // multiply and translate
                }
            

        let intersects = new THREE.Raycaster(this.position, new THREE.Vector3(0, -1, 0)).intersectObjects( this.gamescene.scene.children.filter( o => !(o instanceof THREE.BoxHelper)) );
        this.physicsobject.onground = Boolean(intersects.filter(el => (el.distance > 0.4 && el.distance < 0.5) ).length)
        if (!this.physicsobject.onground) this.physicsobject.downwardvelocity-=0.001;
        else this.physicsobject.downwardvelocity = 0;
        
        this.position.y += this.physicsobject.downwardvelocity;

        return this.position;
    }

    getmeshes(){
        return [this.mesh];
    }

    
}