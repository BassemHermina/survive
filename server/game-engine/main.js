
import {Gamescene} from './Gamescene'
import Stats from 'stats.js';

let gamescene, stats = null; 

init();
animate();

// usefull games to look forward to: 
// galax.io
// brutes.io <3
 
function init() {
    gamescene = new Gamescene();
    
    // stats 
    stats = new Stats();
    stats.showPanel(0);
    document.body.appendChild( stats.dom );
}

function animate() {

    gamescene.camera.update();
    gamescene.renderer.render( gamescene.scene, gamescene.camera.camera );

    requestAnimationFrame( animate );
    stats.update();
}
