export default
{
    globalconf:{
        gamemode: 'ingame',
        pixelratio: 1.0,
        antialias: false, //too much overhead for a simple option!
        useinstancing: false,
        backgroundcolor: 0xffffff,
        mousesensitivity: 1.0,
    },

    // ubuntu: DRI_PRIME=1 google-chrome --disable-software-rasterizer , and reduce window size to 1500 x 900

    worldmap:{
        cameradebug: {x: 1.7, y: 0.8, z: 2}, //only used in gamemode: 'debug'
        camerafirstperson: {x: 0, y: 0.6, z: 0.5},

        alpha:[
            { name: 'box', pos: {x: 0, y: 0, z: 0}, rot: {x: 0, y: 0, z: 0}, scale: {x: 2.9, y: 0.2, z: 2.9} },
            { name: 'box', pos: {x: -0.7, y: 1.45/2 - 0.1, z: -0.7}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.45, y: 1.45, z: 0.45} },
            { name: 'box', pos: {x: 0, y: 1.45/2 - 0.1, z: -1.45}, rot: {x: 0, y: 0, z: 0}, scale: {x: 2.9, y: 1.45, z: 0.2} },
            { name: 'box', pos: {x: 0, y: 1.45-0.05, z: -1.85}, rot: {x: 0, y: 0, z: 0}, scale: {x: 2.9, y: 0.2, z: 1} },
            { name: 'sphere', pos: {x: -0.7, y: 0.5, z: 0.7}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.3, y: 0.3, z: 0.3} },
            // stairs
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.05, z: 0}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.15, z: -0.1}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.25, z: -0.2}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.35, z: -0.3}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.45, z: -0.4}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.55, z: -0.5}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.65, z: -0.6}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.75, z: -0.7}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.85, z: -0.8}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+0.95, z: -0.9}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+1.05, z: -1.0}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+1.15, z: -1.1}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+1.25, z: -1.2}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            { name: 'box', pos: {x: 1.45-0.4, y: 0.1+1.35, z: -1.3}, rot: {x: 0, y: 0, z: 0}, scale: {x: 0.8, y: 0.1, z: 0.1} },
            
            // { name: 'box', pos: {x: Math.random()*6-3, y: Math.random()*6-3, z: Math.random()*6-3}, rot: {x: 0, y: 0, z: 0}, scale: {x: Math.random()/2, y: Math.random()/2, z: Math.random()/2} },
        ]
    }
}