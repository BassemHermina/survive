
/**
 * Gamescene Class
 * @author BassemHermina
 * Date: 7 Sep 2020
 */

import * as THREE from 'three';
import {Camera} from './Camera';
import {Gameobject} from './Gameobject'
import {Loader} from './Loader'
import {Player} from './Player'

import config from './game-config.js'

export class Gamescene {

    /**
     * 1 and having an array of objects to reference all
     * of them, having their meta data regarding how to render and which shader .. etc 
     *  [as a way to handle instancing later? Instancing is a must for players/guns/bullets/worldobjects/fireefx] 
     * 2 This class creates the skybox [added to the array of renderable objects]
     * 3 This class creates all the needed lights 
     * World physics? 
     * Renquesting frames? [after updating then drawing all renderable objects.. how threeJS handles that?]
     */

    /**
     * creates the ThreeJS WebGl context renderer
     * creates the camera object providing it the created renderer 
     * creates the scene object providing it both the renderer and the camera
     */
    constructor(){

        // state 
        this.renderphysicsworld = false;


        ///////////////////////////
        // locals
        this.renderer = null;
        this.camera = null;
        this.scene = null;
        this.gameobjects = [];

        this.intializerenderer();
        this.initializecamera();
        this.initializescene();

        this.buildworld();
        this.createplayer();

        this.inituicontrols();
    }

    intializerenderer(){
        // does the renderer need its own class? instead of being in the Gamescene
        this.renderer = new THREE.WebGLRenderer( { antialias: config.globalconf.antialias } );
        // remember the beautiful option of having a ratio for screen resolution to speed up render (kero's horror steam game)
        this.renderer.setPixelRatio(config.globalconf.pixelratio);
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.appendChild( this.renderer.domElement );
    }

    initializecamera(){
        this.camera = new Camera(this.renderer);
    }

    initializescene(){
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color( config.globalconf.backgroundcolor );
    }

    buildworld(){

        // initialize Gameobjects in the Gamescene class array of renderable Gameobjects
        for(const object of config.worldmap.alpha){
            const dummyobject = new Gameobject();
            if (object.name.split(".").length > 1){
                // has an extension - still TODO more thinking, not now
                // should check the Geometryloader if the geometry for this file is already loaded
                // then create a Gameobject and provide it with the geometry/material
                // ! DONT initialize mesh here, only when all objects are loaded (for instancing)
                // also note: I dont think the player object should be here, also it will directly created as instanced with count = 1
            } else {
                // no extension
                switch(object.name){
                    case "box":
                        dummyobject.geometryBuffer = Loader.boxgeometry_instanced;
                        dummyobject.material = Loader.meshnormalmaterial_instanced;
                        break;
                    case "sphere":
                        dummyobject.geometryBuffer = Loader.spheregeometry_instanced;
                        dummyobject.material = Loader.meshbasicmaterial_instanced;
                        break;
                    default:
                        throw "unkown object type."
                }
                // assign pos, rot, scale, name to the gameobject for assigning them to Three mesh later
                Object.assign(dummyobject, object);
            }
            this.gameobjects.push(dummyobject);
        }

        // then initialize/create all meshes and add them to Three scene
        this.initializemeshes(config.globalconf.useinstancing);

        console.log(this.scene)
    }

    initializemeshes(useinstancing){
        // initialize instancing objects based on objects having same geometry (TODO and material)
        // and add them to scene
        if(useinstancing)
        for(const geobuffer of new Set(this.gameobjects.map(g => g.geometryBuffer))){
            const samebuffer = this.gameobjects.filter(g => geobuffer == g.geometryBuffer)
            if (samebuffer.length > 1){
                const dummyobject = new Gameobject();
                this.scene.add(dummyobject.initializeinstancedmesh(samebuffer));
                this.gameobjects.push(dummyobject);
            }   
        }

        // initialize the rest of meshes and add them to scene
        this.gameobjects.filter(g => (!g.instanceof && g.geometryBuffer)).forEach(go => {
            this.scene.add( go.initializemesh() )
        })
    }

    createplayer(){
        this.myplayer = this.camera.myplayer;
        this.myplayer.gamescene = this;
    }

    inituicontrols(){

        // I think there should be another array for the current logged in player, in which my player should be the first one    
        document.addEventListener( 'keypress', (event) => {
            switch(event.keyCode){
                case 98: // b
                    if (!this.renderphysicsworld){
                        this.renderphysicsworld = true;
                        // this function (viewing the physics world) will need update when there are spheres too, not generic
                        // IMP: this is static and not related to the physics object
                        // the game objects
                        for (let i = 0; i < this.gameobjects.length; i++){
                            this.scene.add(new THREE.BoxHelper(this.gameobjects[i].mesh, 0x00ff00));
                        }
                        
                        // the player
                        let dynamicBH = new THREE.BoxHelper(this.myplayer.mesh, 0x00ff00)
                        this.scene.add(dynamicBH);
                        // hack, add this in the player
                        this.myplayer.physicsboxhelper = dynamicBH;
                    }
                    else {
                        this.renderphysicsworld = false;
                        for(let i = 0; i < this.scene.children.length; i++){
                            if (this.scene.children[i] instanceof THREE.BoxHelper){
                                this.scene.remove(this.scene.children[i])
                                i--;
                            }
                        }
                    }
                    break;
            }
          }, false );

    }

}