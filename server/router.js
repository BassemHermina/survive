
export function router(app){

    app.get('/',function(req, res){
        res.type('text/plain');
        res.send('SurvivorHunt');    
    });

    app.get('/gamescene',function(req, res){
        res.render('main');
    });
}